# frozen_string_literal: true

module Strum
  module Service
    VERSION = "0.2.4"
  end
end
